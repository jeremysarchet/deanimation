"""Useful extras"""

import math
import os
import shutil
import subprocess

from PIL import Image, ImageOps, ImageFilter

from deanimation import FRAME_PATTERN


def extract(input_video, output_directory, filter=None):
    """Extracts all frames from `input_video` with an ffmpeg command.

    Makes `output_directory` and creates image files there whose filenames have
    a numerically increasing pattern given by `frame_patern`.

    `output_directory` must not already exist.
    """
    print("\nExtracting frames from video")

    os.mkdir(output_directory)
    output_frame = os.path.join(output_directory, FRAME_PATTERN)

    arguments = ["ffmpeg",
                 "-i", input_video,
                 "-start_number", "0",
                 output_frame,
                 "-vsync", "vfr",
                 "-hide_banner",
                 "-loglevel", "panic"
                 ]

    if filter:
        print("Extracting with filter: `{}`".format(filter))
        # Insert the filter before the output_frame argument
        i = arguments.index(output_frame)
        arguments.insert(i, "-vf")
        arguments.insert(i + 1, filter)

    subprocess.call(arguments)


def resize(input_video, output_width, output_video=None):
    print("{} - resizing to {}".format(input_video, output_width))

    if not output_video:
        output_dir = os.path.dirname(input_video)
        output_filename = os.path.basename(input_video)[:-4] + "-{}.mp4".format(output_width)
        output_video = os.path.join(output_dir, output_filename)

    subprocess.call(['ffmpeg',
                    '-i', input_video,

                    # Makes web playback happy
                    "-movflags", "faststart",

                    '-vf', 'scale={}:-1'.format(output_width),
                    output_video])


def preview_video(input_video, output_width, output_video):
    print("Generating preview video: {}".format(input_video))

    subprocess.call(['ffmpeg',
                    '-i', input_video,

                    # Make Quicktime and other "dumb" players happy
                    "-pix_fmt", "yuv420p",

                    '-vf', 'scale={}:-1'.format(output_width),

                    "-hide_banner",
                    "-loglevel", "error",

                    output_video])


def encode(input_directory, output_video, framerate, poster_index=0, sharpen=False, curves=None):
    """Given an image sequence, produces a video file.

    Runs an ffmpeg command to take image files from `input_directory` whose
    filenames are numerically increasing and given by `FRAME_PATTERN` and
    creates new `output_video` with given `framerate`.

    The ffmpeg command is setup with the intention of producing a high quality
    output video.

    https://trac.ffmpeg.org/wiki/Encode/H.264
    http://hamelot.io/visualization/using-ffmpeg-to-convert-a-set-of-images-into-a-video/
    """
    print("\n\nEncoding")
    input_frame = os.path.join(input_directory, FRAME_PATTERN)

    arguments = ["ffmpeg",
                    "-r", str(framerate),
                    "-i", input_frame,

                    # Take longer to encode for higher quality
                    # "-preset", "veryslow",

                    # Makes web playback happy
                    "-movflags", "faststart",

                    "-tune", "film",

                    # Constant Rate Factor lower is better quality
                    # "-crf", "10",

                    # Make Quicktime and other "dumb" players happy when
                    # working with BMP images (see first link in docstring).
                    # This seems to backfire when using JPG
                    "-pix_fmt", "yuv420p",

                    "-hide_banner",
                    "-loglevel", "error",

                    output_video,

                    "-hide_banner"]

    if curves:
        curves = generate_curves_string(curves)

    filter = None

    if sharpen and not curves:
        filter = "unsharp"

    elif curves and not sharpen:
        filter = curves

    elif curves and sharpen:
        filter = 'unsharp=5:5:1.0:5:5:0.0, {}'.format(curves)

    if filter:
        print("applying filter: `{}`".format(filter))
        location = arguments.index(output_video)
        arguments.insert(location, "-vf")
        arguments.insert(location + 1, filter)

    subprocess.call(arguments)

    # Make a poster image
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()
    poster_image = Image.open(os.path.join(input_directory, files[poster_index]))
    if sharpen:
        poster_image = poster_image.filter(ImageFilter.SHARPEN)
    poster_image.save(output_video[:-4] + "-poster.png")


def generate_coordinates(points):
    result = ""
    points = [[round((c / 255.0), 2) for c in point] for point in points]
    for i, point in enumerate(points):
        result += "{}/{}".format(point[0], point[1])
        if (i + 1) < len(points):
            result += " "
    return result


def generate_curves_string(data):
    if len(data) == 1:
        all = generate_coordinates(data["all"])
        return "curves=all='{}'".format(all)
    elif len(data) == 3:
        r = generate_coordinates(data["r"])
        g = generate_coordinates(data["g"])
        b = generate_coordinates(data["b"])
        return "curves=r='{}':g='{}':b='{}'".format(r, g, b)
    else:
        return ""


def to_webm(input_video):
    output_video = input_video[:-4] + ".webm"
    subprocess.call(['ffmpeg',
                    '-i', input_video,
                    '-c:v', 'libvpx',
                    '-crf', '10',
                    '-b:v', '1M',
                    '-c:a', 'libvorbis',
                    output_video])


def pretty_preview(input_image_path, output_image_path, scale_up=False):
    """Takes an image at `input_image_path` and saves a copy at
    `output_image_path` which is scaled up and with colors auto leveled.

    Designed to take low-res preview images and make them quickly presentable.
    """
    input_image = Image.open(input_image_path)
    output_image = ImageOps.autocontrast(input_image)
    if scale_up:
        output_image = output_image.resize((1920, 1080))
    output_image.save(output_image_path)


def seamless_loop(input_directory, output_directory, crossfade_duration, front_trim=0, still_start=False, still_end=False):
    """Creates a new sequence where a crossfade transition is placed at the end of
    the sequence so it can be played back as a more or less seamless loop, even
    if the background is not constant.

    Removes `crossfade_duration` frames from the start and blends these frames
    at the end from transparent to opaque.

    `front_trim` how many frames to remove from the start of the sequence.
    This is useful in the case of a tail or fade video where the background is
    not constant (e.g. clouds) which results in a jump at t frames due to the
    tail queue completely crossing over the threshold and no longer spanning
    across the end of the sequence. In this case, best to just discard the first
    t frames and call them a loss. Think of it as needing at least t frames of
    pre-roll in order to have sufficient back material in order to build up a
    tail of length t.
    """

    # Get a list of all the image filenames in the directory, exclude hidden filenames
    filenames = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    filenames.sort()
    filepaths = [os.path.join(input_directory, f) for f in filenames]
    length = len(filenames)

    # Create the main portion (simple abridgement)
    start = crossfade_duration + front_trim if not still_start else 0
    end = length - crossfade_duration if not still_end else length
    abridged_length = end - start

    if abridged_length <= 0:
        raise Exception("This sequence is too short to crossfade. Failed to abridge to [start, end] with values [{}, {}]".format(start, end))

    print("start:{} end:{} abridged length: {}".format(start, end, abridged_length))
    abridge(input_directory, output_directory, [[start, end]])

    if still_end:
        A = Image.open(filepaths[-1])
    if still_start:
        B = Image.open(filepaths[front_trim])

    # Create the crossfade portion
    for i in range(crossfade_duration):
        a = length - crossfade_duration + i if not still_end else length - 1
        b = i + front_trim if not still_start else front_trim

        if not still_end:
            A = Image.open(filepaths[a])
        if not still_start:
            B = Image.open(filepaths[b])

        alpha = i / crossfade_duration
        blend_image = Image.blend(A, B, alpha)
        output_index = abridged_length + i
        blend_image.save(os.path.join(output_directory, FRAME_PATTERN % output_index))
        print("Took frame {} and blended frame {} on top of it to make output frame {} with alpha: {}".format(a, b, output_index, round(alpha, 2)))


def abridge(input_directory, output_directory, keeper_intervals):
    """Takes an image sequence, removes all frames except for keepers defined
    by a list `keeper_intervals` and creates an abridged sequence. The abridged
    sequence has the images renumbered to be contiguous.

    `filename_pattern` is a string with curly brackets for substitution of the
    4 digit index. E.g. "my-video-frame-{}.bmp"

    `keeper_intervals` is a list of pairs of integers defining intervals

        E.g.
        [[12, 15], [30, 32], [55, 58]] which would have the effect of removing
        all frames while keeping frames 12, 13, 14, and 30, 31 and 55, 56, 57.

        So the abridged sequence would contain frames as follows:
        [12, 13, 14, 30, 31, 55, 56, 57]

        But they would be renumbered to be:
        [0, 1, 2, 3, 4, 5, 6, 7]

    Note: an interval is inclusive of the start, exclusive of the end.

    `output_directory` must not already exist
    """
    print("\n\nCreating abridged sequence")

    os.mkdir(output_directory)

    output_index = 0
    keeper_indices = []

    for interval in keeper_intervals:
        start, end = interval
        keeper_indices += (list(range(start, end)))

    for index in keeper_indices:
        filename = FRAME_PATTERN % index
        source_filepath = os.path.join(input_directory, filename)
        copy_filepath = os.path.join(output_directory, "copy of " + filename)

        # Copy and renumber
        shutil.copy(source_filepath, copy_filepath)
        renumbered_filename = FRAME_PATTERN % output_index
        renumbered_filepath = os.path.join(output_directory, renumbered_filename)
        os.rename(copy_filepath, renumbered_filepath)

        print("Took frame {} and created abridged frame {}".format(index, output_index), end="\r")

        output_index += 1

    print()


def abridge_seamless_frame_recipes(keeper_intervals, crossfade_duration):
    """Helper function for `abridge_seamless`.

    Builds up a recipe for each output frame in the sequence.
    For frames contained in an interval, take each of those frames as-is
    For the crossfades, blend the needed intermediate frames together with a
    smooth ramp of alpha.

    e.g.

        keeper_intervals = [[10, 13], [55, 58]]

        would result in the following value for `frame_recipes`:
            [('plain', 10),
            ('plain', 11),
            ('plain', 12),
            ('blend', [13, 51, 0.2]),
            ('blend', [14, 52, 0.4]),
            ('blend', [15, 53, 0.6]),
            ('blend', [16, 54, 0.8]),
            ('plain', 55),
            ('plain', 56),
            ('plain', 57),
            ('blend', [58, 6, 0.2]),
            ('blend', [59, 7, 0.4]),
            ('blend', [60, 8, 0.6])]
    """
    print("\n\nCreating seamless abridged sequence")

    frame_recipes = []

    # Ensure that all the gaps are sufficiently large to accommodate crossfades
    for interval in keeper_intervals:
        i = keeper_intervals.index(interval)

        # Accommodate the final crossfade that wraps around to the beginning
        if i == 0:
            gap = interval[0]
            if gap < crossfade_duration * 2:
                raise Exception("The first interval {} needs to start no earlier than {}".format(interval, crossfade_duration * 2))

        # Accommodate interstitial crossfades
        else:
            previous_interval = keeper_intervals[i - 1]
            gap = interval[0] - previous_interval[1]

            if gap < crossfade_duration * 2:
                raise Exception("The gap between intervals {} and {} needs to be at least {}".format(previous_interval, interval, crossfade_duration * 2))

    for interval in keeper_intervals:
        i = keeper_intervals.index(interval)

        # Get a handle to the next interval. At the end wrap back to the first
        if i < len(keeper_intervals) - 1:
            next_interval = keeper_intervals[i + 1]
        else:
            next_interval = keeper_intervals[0]

        # Add the plain frame recipes for this interval
        for j in range(interval[0], interval[1]):
            frame_recipes.append(("plain", j))

        # Add the blended frame recipes between this interval and next
        for j in range(crossfade_duration):
            alpha = (j + 1) / (crossfade_duration + 1)
            frame_recipes.append(("blend", [interval[1] + j, next_interval[0] - crossfade_duration + j, alpha]))

    return frame_recipes


def abridge_seamless(input_directory, output_directory, keeper_intervals, crossfade_duration):
    """Like `abridge` but adds crossfades in between the jumps.

    `keeper_intervals` needs to have length of at least 2
    """

    os.mkdir(output_directory)

    # Get a list of all the image filenames in the directory, exclude hidden filenames
    filenames = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    filenames.sort()
    filepaths = [os.path.join(input_directory, f) for f in filenames]

    frame_recipes = abridge_seamless_frame_recipes(keeper_intervals, crossfade_duration)

    for frame in frame_recipes:
        output_index = frame_recipes.index(frame)

        type, recipe = frame[0], frame[1]

        if type == "plain":
            index = recipe
            filename = FRAME_PATTERN % index
            source_filepath = os.path.join(input_directory, filename)
            copy_filepath = os.path.join(output_directory, filename)

            # Copy and renumber
            shutil.copy(source_filepath, copy_filepath)
            renumbered_filename = FRAME_PATTERN % output_index
            renumbered_filepath = os.path.join(output_directory, renumbered_filename)
            os.rename(copy_filepath, renumbered_filepath)
            print("Took frame {} and copied it as frame {}".format(index, output_index))

        elif type == "blend":
            a_index, b_index, alpha = recipe
            A = Image.open(filepaths[a_index])
            B = Image.open(filepaths[b_index])

            # Blend and save
            blend_image = Image.blend(A, B, alpha)
            blend_image.save(os.path.join(output_directory, FRAME_PATTERN % output_index))
            print("Took frame {} and blended frame {} on top of it to make output frame {} with alpha: {}".format(a_index, b_index, output_index, round(alpha, 2)))


def offset(input_directory, output_directory, offset):
    """Takes an image sequence, and creates a new sequence with the same
    ordering, but starting at `offset` and looping back around to 0 and completing
    the loop with the final frame being offset-1. The new sequence frames are
    renumbered starting at 0

    E.g. with a sequence of length 10, and an offset of 4, the resulting sequence
    will consist of frames: [4, 5, 6, 7, 8, 9, 0, 1, 2, 3], and renumbered to
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    `output_directory` must not already exist
    """
    print("\n\nCreating abridged sequence")

    filenames = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    original_indices = list(range(len(filenames)))

    os.mkdir(output_directory)

    output_index = 0
    offset_indices = original_indices[offset:] + original_indices[:offset]

    for index in offset_indices:
        filename = FRAME_PATTERN % index
        source_filepath = os.path.join(input_directory, filename)
        copy_filepath = os.path.join(output_directory, filename)

        # Copy and renumber
        shutil.copy(source_filepath, copy_filepath)
        renumbered_filename = FRAME_PATTERN % output_index
        renumbered_filepath = os.path.join(output_directory, renumbered_filename)
        os.rename(copy_filepath, renumbered_filepath)

        print("Took frame {} and created offset frame {}".format(index, output_index))

        output_index += 1


def level(input_directory, output_directory, rotation, size):
    """Takes an image sequence, rotates and and scales up as needed, creating
    a new image sequence with no gaps in the corners due to the rotation."""

    print("\n\nCreating leveled sequence")

    os.mkdir(output_directory)

    image = None

    # Get a list of all the image files in the directory, exclude hidden files
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()

    for file in files:
        i = files.index(file)
        msg = "Leveling frame {} of {} by {} degrees. {}".format(i + 1, len(files), rotation, file)
        filename = os.path.join(input_directory, file)

        image = Image.open(filename)
        image = image.rotate(rotation, resample=Image.BICUBIC)

        scale = scale_for_rotate_and_circumscribe(rotation, size[0], size[1])
        new_size = (math.ceil(size[0] * scale), math.ceil(size[1] * scale))
        image = image.resize(new_size, resample=Image.BICUBIC)

        x = (image.width - size[0]) // 2
        y = (image.height - size[1]) // 2
        image = image.crop((x, y, size[0] + x, size[1] + y))

        image.save(os.path.join(output_directory, FRAME_PATTERN % i))

        print(msg)


def scale_for_rotate_and_circumscribe(rotation, width, height):
    """Returns a factor by which to scale the frame when rotated
    so as to circumscribe the original video dimensions.
    """
    alpha = abs(rotation) * math.pi/180

    beta = math.atan(height / width)
    gamma = math.pi - alpha - beta

    factor = math.sin(gamma) / math.sin(beta)

    return factor


if __name__ == "__main__":

    # # Test for curves
    # curves = {"all":[[0, 0], [84, 100], [171, 255]]}
    # curves = {"all":[[0, 0], [128, 89], [255, 255]],
    #             "r":[[0, 0], [115, 255]],
    #             "g":[[0, 0], [147, 255]],
    #             "b":[[0, 0], [164, 255]]}
    # print(generate_curves_string(curves))

    # # Test for abridgement
    # input_dir = "processed/0241/frames-extracted"
    # filename_pattern = "0241-frame-{}.bmp"
    # intervals = [[12, 15], [30, 32], [55, 58]]
    # output_dir = "processed/0241/frames-abridged"
    # abridge(input_dir, filename_pattern, intervals, output_dir)

    # # Test for seamless_loop
    # input_directory = "test-sequence"
    # output_directory = "test-sequence-output"
    # output_video = "seamless-test.mp4"
    # seamless_loop(input_directory, output_directory, 30, front_trim=15)

    # Test for abridge_seamless_frame_recipes
    # keeper_intervals = [[10, 13], [55, 58]]
    # crossfade_duration = 4
    # frame_recipes = abridge_seamless_frame_recipes(keeper_intervals, crossfade_duration)
    # for frame in frame_recipes:
    #     print(frame)

    print(scale_for_rotate_and_circumscribe(45, 200, 200))
