import fnmatch
import os
import sys

YEAR = 2020

sn = sys.argv[1]

raw_video = "/Users/jbs/Dropbox/deanimation/raw/{}/DSCF{}.MOV".format(YEAR, sn)
if not os.path.exists(raw_video):
    raw_video = None

previews_dir = "/Users/jbs/Dropbox/deanimation/previews/{}".format(YEAR)
preview_image = None
for file in os.listdir(previews_dir):
    if fnmatch.fnmatch(file, "*{}*.png".format(sn)):
        preview_image = os.path.join(previews_dir, file)

previews_pretty_dir = "/Users/jbs/Dropbox/deanimation/previews-pretty/{}".format(YEAR)
preview_pretty_image = None
for file in os.listdir(previews_pretty_dir):
    if fnmatch.fnmatch(file, "*{}*.png".format(sn)):
        preview_pretty_image = os.path.join(previews_pretty_dir, file)

answer = None
while answer not in ["y", "n"]:
    print("Found the following raw video and previews for {}".format(sn))
    print(raw_video)
    print(preview_image)
    print(preview_pretty_image)
    answer = input("Are you sure you want to remove {}? [y/n]?".format(sn)).lower()

if answer == "y":
    if raw_video is not None:
        os.remove(raw_video)
    if preview_image is not None:
        os.remove(preview_image)
    if preview_pretty_image is not None:
        os.remove(preview_pretty_image)
