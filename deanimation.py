"""Tools for producing deanimation images and videos


Intended use:

1. Given a source video, `extract` creates a sequence of its frames

2. Then you've got an image sequence from which `accumulation_single` creates
a single deanimation image.

3. From that same image sequence sequence, `accumulation_sequence` creates
a new special-effect image sequence which visualizes the progression of
accumulated blends.

4. Then you've got an accumulation sequence from which `encode` creates an
accumulation video.


The recommended workflow is to use bitmap (.bmp) for the image sequences.

- .jpg has the advantage of being fast but leaving artifacts in the accumulation
- .png has the advantage of being artifact-free but taking a long time and
requiring a lot of memory due to the lossless compression required.
- .bmp is fast and artifact-free but does require a lot of storage space.

.bmp wins out because it has the advantage of being fast and free from
compression. The directories of .bmp files can be treated as intermediary and
can be deleted upon completion.
"""

import os
import subprocess
from PIL import Image, ImageChops
import custom_blends

FRAME_PATTERN = "%04d.bmp"

DARKEN = 0
LIGHTEN = 1
DUAL = 2
FURTHER_FROM = 3
CHANNEL = 4
DARKER_COLOR = 5
LIGHTER_COLOR = 6

ACCUMULATION = 0
TAIL = 1
FADE = 2

def blend_many(mode, images):
    """Returns the blend of every image in `images` together."""
    result = images[0]
    for image in images[1:]:
        result = blend(mode, result, image)
    return result


def blend(mode, image1, image2, reference_image=None):
    blend_image = None
    if mode == LIGHTEN:
        blend_image = ImageChops.lighter(image1, image2)
    elif mode == DARKEN:
        blend_image = ImageChops.darker(image1, image2)
    elif mode == FURTHER_FROM:
        blend_image = custom_blends.further_from_image(image1, image2, reference_image)
    elif mode == CHANNEL:
        blend_image = custom_blends.channel(image1, image2)
    elif mode == DARKER_COLOR:
        blend_image = custom_blends.darker_color(image1, image2)
    elif mode == LIGHTER_COLOR:
        blend_image = custom_blends.lighter_color(image1, image2)

    return blend_image


def accumulation_single(input_directory, output_image, mode, n, reference_image=None):
    """Produce a single image which is the blend of an entire sequence.

    Given a sequence of image files in `input_directory` whose filenames have
    a numerically increasing pattern given by `FRAME_PATTERN`, creates
    a single image by iterating through the sequence and performing intermediate
    blends, each time blending the current with the cumulative blend of all
    previous. Then, creates the final image whose filename is `output_image`

    Applies a lighten or a darken blend based on `mode`; lighten if `mode`
    evalutaes to True, darken otherwise.

    Only blends every nth frame from the input sequence given by `n`.
    """

    if mode == DUAL:
        output_lightened_path = output_image[:-4] + "-lightened" + output_image[-4:]
        output_darkened_path = output_image[:-4] + "-darkened" + output_image[-4:]
        accumulation_single(input_directory, output_lightened_path, 1, n)
        accumulation_single(input_directory, output_darkened_path, 0, n)
        output_lightened = Image.open(output_lightened_path)
        output_darkened = Image.open(output_darkened_path)
        overlayed_image = custom_blends.overlay(output_lightened, output_darkened)
        overlayed_image.save(output_image)
        return

    print("\n\nCreating accumulation single")

    previous_image = None
    image = None

    # Get a list of all the image files in the directory, exclude hidden files
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()

    for file in files:
        i = files.index(file)
        print("Blending image {} of {}".format(i+1, len(files)), end="\r")
        if i % n == 0:
            filename = os.path.join(input_directory, file)
            image = Image.open(filename)
            if not previous_image:
                previous_image = image

            # Perform the intermediate blend
            blend_image = blend(mode, image, previous_image, reference_image)

            # Assign this intermediate image to the previous image for the
            # next iteration
            previous_image = blend_image

    # After iteration, save the final output image
    blend_image.save(output_image)
    print()


def accumulation_sequence(input_directory, output_directory, mode, n, reference_image=None):
    """From an image sequence, produces a new sequence with blend accumulation

    Makes `output_directory` and produces a new sequence of image files there
    whose filenames have a numerically increasing pattern given by
    `FRAME_PATTERN`. `output_directory` must not already exist.

    Produces the new image sequence by iterating through the image files
    from `input_directory` in alphanumeric order, and producing each new frame
    by blending the current frame with the cumulative blend of all previous
    frames.

    Applies a lighten or a darken blend based on `mode`; lighten if `mode`
    evalutaes to True, darken otherwise.

    Accumulates every nth input frame into the output sequence given by `n`.
    E.g. pass a value of 1 to accumulate every frame. Or a
    value of 4 to accumulate every fourth frame. The latter case is a fancy
    way of preserving all the original frames of the input sequence (thus
    avoiding choppy playback) while enabling the tracers to only "leave an
    imprint" on the cumulative background blend every nth frame.
    """
    os.mkdir(output_directory)

    if mode == DUAL:
        output_lightened_directory = output_directory + "-lightened"
        output_darkened_directory = output_directory + "-darkened"
        accumulation_sequence(input_directory, output_lightened_directory, 1, n)
        accumulation_sequence(input_directory, output_darkened_directory, 0, n)
        custom_blends.overlay_sequence(output_lightened_directory, output_darkened_directory, output_directory)
        return

    print("\n\nCreating accumulation sequence")

    previous_image = None
    image = None

    # Get a list of all the image files in the directory, exclude hidden files
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()

    for file in files:
        i = files.index(file)
        msg = "Accumulating frame {} of {}. {}".format(i + 1, len(files), file)
        filename = os.path.join(input_directory, file)
        image = Image.open(filename)
        if not previous_image:
            previous_image = image

        # Perform the blend
        blend_image = blend(mode, image, previous_image, reference_image)
        blend_image.save(os.path.join(output_directory, FRAME_PATTERN % i))

        # Only accumulate a new imprint to the blend every Nth frame
        if i % n == 0:
            previous_image = blend_image
            msg += " *"
        else:
            msg += "  "

        print(msg, end="\r")
    print()


def tail_sequence(input_directory, output_directory, mode, n, t, discard_lead_up=False):
    """From an image sequence, produces a new sequence with tail blending.

    Just like the function `accumulation_sequence`, but instead of always
    accumulating the blends, for the ith frame, make a blend of the m previous
    frames.

    This way, the moving subject has a "tail" of constant length of the
    previous m blends.

    `t` is the length of the tail, not in frames but in blends.

    `n` is the same as from `accumulation_sequence`, blend every n frames.
    """
    print("\n\nCreating tail sequence")

    offset = n * t if discard_lead_up else 0

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    # Get a list of all the image files in the directory, exclude hidden files
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()

    # Truncate the end of the list so that it has length of a multiple of n
    if n > 1:
        while len(files) % n != 1:
            removed = files.pop()
            print("Ignoring: {} so sequence length is a multiple of {}n + 1".format(removed, n))
        print("Now the sequence of images is length: {}".format(len(files)))

    # Get a handle to the final image and pop it from the list
    # Now the array of images will be of length of a multiple of n
    terminus_file = files.pop(-1)
    print("Without the ternimus image, the sequence is length: {}\n".format(len(files)))

    # Setup an array to store the past t tail images
    tail_images = []
    for i in range(t):
        b = offset - ((i + 1) * n)
        # print("Enqueuing files[{}] into tail images".format(b))
        image = Image.open(os.path.join(input_directory, files[b]))
        tail_images.append(image)

    # Generate the initial blend of all the tail images
    blended_tail = blend_many(mode, tail_images)

    # Iterate through the sequence
    for i in range(offset, len(files)):
        file = files[i]
        msg = "Processed file {} to make frame {} of {}.".format(file, i - offset, len(files) - offset)

        # The current image is the leader
        image = Image.open(os.path.join(input_directory, file))

        # Blend in the tail
        blend_image = blend(mode, image, blended_tail)

        # Blend in the follower
        follower_file = files[i - (n * t)]
        follower_image = Image.open(os.path.join(input_directory, follower_file))
        blend_image = blend(mode, blend_image, follower_image)

        # Blend in the (i * n + 1)th image, the -1th image, the last image
        # This makes for a nicer effect in the case with a tail which is split
        # over the end of a clip (the loop seam)
        if i < (n * t):
            terminus_image = Image.open(os.path.join(input_directory, terminus_file))
            blend_image = blend(mode, blend_image, terminus_image)

        # Save the final blend image
        blend_image.save(os.path.join(output_directory, FRAME_PATTERN % (i - offset)))

        # Only advance and regenerate the tail every Nth frame
        if i % n == 0:
            tail_images.pop(-1)  # remove the furthest back (tip) tail image from the end of the array
            tail_images.insert(0, image)  # add this image to the most recent (base) tail image at the start of the array
            blended_tail = blend_many(mode, tail_images)
            msg += " *"
        else:
            msg += "  "

        print(msg, end="\r")

    print()


def fade_sequence(input_directory, output_directory, mode, n, t, r):
    """From an image sequence, produces a new sequence with tail blending and
    where the tail fades.

    Just like the function `tail_sequence`, but instead of the tail remaining
    solid throughout its span, it gradually fades to transparent.

    The tail images are blended with 100% opacity for a solid section, then
    gradually taper to 0% opacity in the taper section.

    `n` is the same as from `accumulation_sequence`, blend every n frames.

    `t` is the length of the tail, not in frames but in blends.

    `r` is the number of images at the end of the tail which are part of the
    ramp from 100% down to 0% opacity
    """
    print("\n\nCreating fade sequence")

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    # Get a list of all the image filenames in the directory, exclude hidden filenames
    filenames = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    filenames.sort()

    filepaths = [os.path.join(input_directory, f) for f in filenames]

    # Truncate the end of the list so that it has length of a multiple of n
    if n > 1:
        while len(filenames) % n != 0:
            removed = filenames.pop()
            print("Ignoring: {} so sequence length is a multiple of {}n".format(removed, n))
        print("Now the sequence of images is length: {}".format(len(filenames)))

    # Setup an array to store the past t tail frames
    tail_frames = []
    for i in range(t):
        b = -((i + 1) * n)
        # image = Image.open(os.path.join(input_directory, filenames[b]))
        # tail_frames.append(image)

        # Open as a file object, load the Image object from the file object
        filepath = os.path.join(input_directory, filenames[b])
        file = open(filepath, 'rb')
        image = Image.open(file)
        image.load()

        tail_frames.append({"filepath": filepath, "file": file, "image": image})

    # Iterate through the sequencew
    for filepath in filepaths:
        i = filepaths.index(filepath)

        # Define a way of fading an equal division each frame. So in addition to
        # the tail members themselves decreasing along the length of the tail
        # in alpha, for every frame, they will also decrease an incremental amount
        # equal to 1/nth of the distance between tail images
        tween = i % n  # How far in-between tail images
        if tween == 0:
            tween = n  # Make n coincide with a tail image, not 0
        delta = ((n - tween) / n) / r # the frame-by-frame marginal change to the alpha along the tail images

        msg = "Processing file {} to make frame {} of {}:".format(os.path.basename(filepath), i, len(filepaths))

        # Open as a file object, load the Image object from the file object
        file = open(filepath, 'rb')
        image = Image.open(file)
        image.load()

        blend_image = image # (leader)

        # Compute the length of the stem of the tail, where images have 100% opacity
        s =  t - r

        # Perform the blend with the tail images
        for tail_frame in tail_frames:
            tail_image = tail_frame["image"]
            j = tail_frames.index(tail_frame)
            if (j <= s):
                # Solid portion at start of tail
                # print("Now on solid tail segment {} of {}. Alpha: 1.0".format(j, s))
                blend_image = blend(mode, blend_image, tail_image)
            else:
                # Portion at end of tail which ramps to 0% opacity
                A = blend_image
                B = blend(mode, blend_image, tail_image)
                alpha = 1.0 - (((j - s) + 1) / r)
                alpha += delta
                # print("Now on ramp tail segment {} of {}. Alpha: {}".format((j - s), r, round(alpha, 3)))
                blend_image = Image.blend(A, B, alpha)

        # Save the final blend image
        blend_image.save(os.path.join(output_directory, FRAME_PATTERN % i))
        blend_image.close()
        blend_image = None

        # Only advance the tail every Nth frame
        if i % n == 0:
            popped_frame = tail_frames.pop(-1)  # remove the furthest back (tip) tail image from the end of the array
            popped_frame["file"].close()

            new_tail_frame = {"filepath": filepath, "file": file, "image": image}
            tail_frames.insert(0, new_tail_frame)  # add this image to the most recent (base) tail image at the start of the array
            msg += " *"
        else:
            image.close()
            msg += "  "

        image = None
        print(msg, end="\r")
    print()


def multiplex(input_directory, output_directory, mode, factor, reference_image=None):
    os.mkdir(output_directory)

    if mode == DUAL:
        output_lightened_directory = output_directory + "-lightened"
        output_darkened_directory = output_directory + "-darkened"
        multiplex(input_directory, output_lightened_directory, 1, factor)
        multiplex(input_directory, output_darkened_directory, 0, factor)
        custom_blends.overlay_sequence(output_lightened_directory, output_darkened_directory, output_directory)
        return

    print("\n\nMultiplexing with a factor of {}".format(factor))

    previous_image = None
    image = None

    # Get a list of all the image files in the directory, exclude hidden files
    files = [f for f in os.listdir(input_directory) if not f.startswith(".")]
    files.sort()

    # Truncate the end of the list so that it has length of a multiple of n
    print("The sequence of images is originally length: {}".format(len(files)))
    while len(files) % factor != 0:
        files.pop()
    print("Now the sequence of images is length: {}".format(len(files)))

    # The new output length is a fraction of the original length
    output_length = int(len(files) / factor)

    output_index = 0
    for file in files[:output_length]:
        i = files.index(file)
        print("Processing multiplex frame {} of {}. {}".format(i + 1, output_length, file), end="\r")

        # Blend with the other corresponding images from the other fractions
        for j in range(factor):
            multiplex_index = i + j * output_length
            # print("Multiplexing with {}".format(i + j * output_length))

            filename = os.path.join(input_directory, files[multiplex_index])
            image = Image.open(filename)
            if not previous_image:
                previous_image = image

            # Perform the blend
            blend_image = blend(mode, image, previous_image, reference_image)

        previous_image = None
        blend_image.save(os.path.join(output_directory, FRAME_PATTERN % i))
    print()
