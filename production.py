"""Jeremy Sarchet Deanimation Production."""

import os

import custom_blends
import deanimation
import media
import utilities

BASE_INPUT_DIR = "/Volumes/mondrian/Dropbox/deanimation/raw"
BASE_WORKING_DIR = "/Users/jbs/deanimation-scratch"
BASE_OUTPUT_DIR = "/Users/jbs/Dropbox/deanimation/processed"


class Sequence:
    """Steps to take a raw video and generate a series of processed image
    sequences to export a processed deanimation video"""

    def __init__(self, year, serial_number, prefix, framerate, blend_mode, deanimation_type, n=1 , t=1, r=0, extension=".MOV", hours_offset=0, explicit_date="", reference_image=None, blur_before_blend=10):
        self.input_video_path = os.path.join(BASE_INPUT_DIR, str(year), "{}{}{}".format(prefix, serial_number, extension))

        date = explicit_date if explicit_date else media.get_encoded_date(self.input_video_path, hours_offset)
        self.identifier = "{}-{}".format(date, serial_number)

        self.working_directory = os.path.join(BASE_WORKING_DIR, str(year), self.identifier)
        self.output_directory = os.path.join(BASE_OUTPUT_DIR, str(year), self.identifier)

        for dir in [self.working_directory, self.output_directory]:
            if not os.path.isdir(dir):
                os.makedirs(dir)

        self.working_directories = {"extracted": "01-extracted",
                                    "abridged": "02-abridged",
                                    "multiplexed": "03-multiplexed",
                                    "leveled": "04-leveled",
                                    "deanimated": "05-deanimated",
                                    "crossfaded": "06-crossfaded",
                                    "offset": "07-offset"}

        for key in self.working_directories:
            self.working_directories[key] = os.path.join(self.working_directory, self.working_directories[key])

        self.resolution = media.get_resolution(self.input_video_path)

        self.framerate = framerate
        self.blend_mode = blend_mode
        self.deanimation_type = deanimation_type
        self.n = n
        self.t = t
        self.r = r
        self.blur_before_blend = blur_before_blend
        self.reference_image = reference_image if reference_image else os.path.join(self.working_directories["extracted"], "0000.bmp")

    def encode_raw_preview(self, resolution=1280):
        preview_video_path = os.path.join(self.output_directory, self.identifier + "-preview-{}.mp4".format(resolution))
        utilities.preview_video(self.input_video_path, resolution, preview_video_path)

    def extract(self, curves={"all":[[0, 0], [255, 255]]}, saturation=None, downsize=False, every_other=False, trim=[]):
        """Decode the video into an image sequence.

        Trim should be a pair of integers indicating which frame to start and end on.
        """
        filter = utilities.generate_curves_string(curves)

        if saturation:
            filter += ", eq=saturation={}".format(saturation)
        if downsize:
            filter += ", scale=1920x1080"

        select_expressions = []
        if every_other:
            select_expressions.append("not(mod(n\,2))")
        if trim:
            select_expressions.append("between(n\,{}\,{})".format(trim[0], trim[1]))
        if select_expressions:
            filter += ", select='" + "*".join(select_expressions) + "'"

        utilities.extract(self.input_video_path, self.working_directories["extracted"], filter)

    def deanimation_still(self, source="extracted"):
        source_directory = self.working_directories[source]
        output_filename = self.identifier + "-still-n{}.png".format(self.n)
        output_image_path = os.path.join(self.output_directory, output_filename)
        deanimation.accumulation_single(source_directory, output_image_path, self.blend_mode, self.n)

    def abridge(self, keeper_intervals, seamless=False, crossfade_duration=10):
        source_directory = self.working_directories["extracted"]
        output_directory = self.working_directories["abridged"]
        if seamless:
            utilities.abridge_seamless(source_directory, output_directory, keeper_intervals, crossfade_duration)
        else:
            utilities.abridge(source_directory, output_directory, keeper_intervals)

    def multiplex(self, factor, source="abridged"):
        source_directory = self.working_directories[source]
        output_directory = self.working_directories["multiplexed"]
        deanimation.multiplex(source_directory, output_directory, self.blend_mode, factor, self.reference_image)

    def level(self, rotation, source="abridged"):
        source_directory = self.working_directories[source]
        output_directory = self.working_directories["leveled"]
        utilities.level(source_directory, output_directory, rotation, self.resolution)

    def encode_concise(self, source="abridged", sharpen=False):
        source_directory = self.working_directories[source]
        output_video_path = os.path.join(self.output_directory, self.identifier + "-original.mp4")
        utilities.encode(source_directory, output_video_path, self.framerate, sharpen)

    def deanimate(self, source="abridged"):
        source_directory = self.working_directories[source]
        output_directory = self.working_directories["deanimated"]

        if source == "multiplexed" and self.blend_mode == deanimation.DUAL:
            os.mkdir(output_directory)
            source_lightened_directory = os.path.join(self.working_directory, "03-multiplexed-lightened")
            output_lightened_directory = os.path.join(self.working_directory, "04-deanimated-lightened")
            source_darkened_directory = os.path.join(self.working_directory, "03-multiplexed-darkened")
            output_darkened_directory = os.path.join(self.working_directory, "04-deanimated-darkened")
            self.deanimation_generator(source_lightened_directory, output_lightened_directory, deanimation.LIGHTEN)
            self.deanimation_generator(source_darkened_directory, output_darkened_directory, deanimation.DARKEN)
            custom_blends.overlay_sequence(output_lightened_directory, output_darkened_directory, output_directory)
            return

        self.deanimation_generator(source_directory, output_directory, self.blend_mode)

    def deanimation_generator(self, source_directory, output_directory, blend_mode):
        if self.deanimation_type == deanimation.ACCUMULATION:
            deanimation.accumulation_sequence(source_directory, output_directory, blend_mode, self.n, self.reference_image, self.blur_before_blend)
        elif self.deanimation_type == deanimation.TAIL:
            deanimation.tail_sequence(source_directory, output_directory, blend_mode, self.n, self.t, discard_lead_up=True)
        elif self.deanimation_type == deanimation.FADE:
            deanimation.fade_sequence(source_directory, output_directory, blend_mode, self.n, self.t, self.r)

    def crossfade(self, source="deanimated", crossfade_duration=48, front_trim=0, still_start=False, still_end=False):
        source_directory = self.working_directories[source]
        output_directory = self.working_directories["crossfaded"]
        if not front_trim and self.deanimation_type == deanimation.TAIL:
            front_trim = self.n * self.t
        utilities.seamless_loop(source_directory, output_directory, crossfade_duration, front_trim, still_start, still_end)

    def offset(self, offset, source="crossfaded"):
        source_directory = self.working_directories[source]
        output_directory = self.working_directories["offset"]
        utilities.offset(source_directory, output_directory, offset)

    def encode_result(self, source="deanimated", poster_frame=0, sharpen=False, curves=None, start_frame=0):
        suffix = "" if source == "deanimated" else "-" + source
        source_directory = self.working_directories[source]
        if self.deanimation_type == deanimation.ACCUMULATION:
            output_filename = self.identifier + "-accumulation-n{}{}.mp4".format(self.n, suffix)
        elif self.deanimation_type == deanimation.TAIL:
            output_filename = self.identifier + "-tail-n{}-t{}{}.mp4".format(self.n, self.t, suffix)
        elif self.deanimation_type == deanimation.FADE:
            output_filename = self.identifier + "-fade-n{}-t{}-r{}{}.mp4".format(self.n, self.t, self.r, suffix)
        output_video_path = os.path.join(self.output_directory, output_filename)

        utilities.encode(source_directory, output_video_path, self.framerate, poster_frame, sharpen, curves)
