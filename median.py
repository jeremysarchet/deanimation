import custom_blends

# # Seagulls over brown field median blend
# input_directory = "/Volumes/noguchi/scratch/2020/2020-01-20-0064/01-extracted-m1/"
# processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2020/2020-01-20-0064"
# reference_image = "{}median-background.png".format(processed_directory)
# files = []
# for i in range(1000):
#     if i % 10 == 0:
#         files.append("{}{}.png".format(input_directory, i))
# custom_blends.median(files, reference_image)

# # Leaves by divinity school intersection median blend
# input_directory = "/Users/jbs/Dropbox/deanimation/preprocessed/2020/0408-abridged-stabilized/frames/"
# processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2020/2020-10-16-0408/"
# reference_image = "{}median-background.png".format(processed_directory)
# files = []
# for i in range(1, 4100):
#     if i % 40 == 0:
#         files.append("{}{:04d}.png".format(input_directory, i))
# custom_blends.median(files, reference_image)

# # Leaves falling in front of Lion wall
# input_directory = "/Volumes/noguchi/scratch/2020/2020-10-25-0440/01-extracted-m1/"
# processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2020/2020-10-25-0440/"
# reference_image = "{}median-background.png".format(processed_directory)
# files = []
# for i in range(4100):
#     if i % 41 == 0:
#         files.append("{}{:d}.bmp".format(input_directory, i))
# custom_blends.median(files, reference_image)

# # Debris falling from forest canopy
# input_directory = "/Volumes/noguchi/scratch/2020/2020-10-27-0459/01-extracted-m1/"
# processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2020/2020-10-27-0459/"
# reference_image = "{}median-background.png".format(processed_directory)
# files = []
# for i in range(3000):
#     if i % 30 == 0:
#         files.append("{}{:d}.bmp".format(input_directory, i))
# custom_blends.median(files, reference_image)

# # Swallowtail butterfly at Tilden
# input_directory = "/Volumes/noguchi/scratch/2017/2017-07-09-9488/01-extracted-m1/"
# processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2017/2017-07-09-9488/"
# reference_image = "{}median-background.png".format(processed_directory)
# files = []
# for i in range(200):
#     if i % 2 == 0:
#         files.append("{}{:d}.bmp".format(input_directory, i))
# custom_blends.median(files, reference_image)

# Water skimmers wide shot
input_directory = "/Volumes/noguchi/scratch/2017/2017-07-09-9519/01-extracted-m1/"
processed_directory = "/Users/jbs/Dropbox/deanimation/processed/2017/2017-07-09-9519/"
reference_image = "{}median-background.png".format(processed_directory)
files = []
for i in range(950):
    if i % 9 == 0:
        files.append("{}{:d}.bmp".format(input_directory, i))
custom_blends.median(files, reference_image)
