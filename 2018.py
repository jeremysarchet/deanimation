
import deanimation
import production

# Dust with desk joint closeup
# s = Sequence(2018, "0100", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=3)
# s.extract(curves = {"r":[[30, 0], [130, 121], [232, 255]],
#                     "g":[[3, 0], [216, 255]],
#                     "b":[[7, 0], [224, 255]]})
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30)

# # Embers in wood stove
# s = Sequence(2018, "0139", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1)
# s.extract(curves={"all":[[0, 0], [61, 115], [120, 255]]})
# s.abridge([[60, 1800]])
# s.multiplex(4, source="abridged")
# s.encode_concise(source="multiplexed")
# s.deanimate(source="multiplexed")
# s.crossfade(source="deanimated", still_end=True, still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30)

# Mosquitos out the third floor window
# s = Sequence(2018, "0152", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=1)
# s.extract(curves={"all":[[30, 0], [76, 107], [122, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated", crossfade_duration=60, still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Seagulls at Sants Estacio
# keeper_intervals = [[4262, 5557], [20, 352], [688, 763], [975, 1285], [1435, 1604], [1995, 2403], [2488, 2996], [3293, 4085], [4120, 4181]]
# s = Sequence(2018, "0242", "IMG_", "29.97", deanimation.FURTHER_FROM, deanimation.ACCUMULATION, n=4)
# s.encode_raw_preview()
# s.extract()
# s.abridge(keeper_intervals)
# s.multiplex(4)
# s.encode_concise(source="multiplexed")
# s.deanimate(source="multiplexed")
# s.crossfade(still_start=True, still_end=True)
# s.encode_result(source="crossfaded", poster_frame=-30)

# # Seagulls in a mass with some in the sky above
# s = Sequence(2018, "0543", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=4, explicit_date="2018-04-02", extension="-stabilized.mov")
# s.extract(curves={"all":[[47, 0], [121, 119], [193, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # # Seagulls over distant sea horizon with rock in bottom-right
# s = Sequence(2018, "0544", "MVI_", 24, deanimation.DUAL, deanimation.ACCUMULATION, n=4, explicit_date="2018-04-02", extension="-stabilized.mov")
# s.extract(curves={"all":[[18, 0], [118, 161], [215, 255]]})
# s.multiplex(2, source="extracted")
# s.encode_concise(source="multiplexed", sharpen=True)
# s.deanimate(source="multiplexed")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Seagulls from a rock shelf overlook
# s = Sequence(2018, "0556", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=4, explicit_date="2018-04-02", extension="-stabilized.mov")
# s.extract(curves={"all":[[45, 0], [122, 154], [194, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # A few seagulls beyond a surreal rock formation
# s = Sequence(2018, "0564", "MVI_", 24, deanimation.DUAL, deanimation.ACCUMULATION, n=4, explicit_date="2018-04-02", extension="-stabilized.mov")
# s.extract()
# s.multiplex(2, source="extracted")
# s.encode_concise(source="multiplexed", sharpen=True)
# s.deanimate(source="multiplexed")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True, curves={"all":[[12, 0], [169, 255]]})

# # One seagull lands on a spire, displacing another
# s = Sequence(2018, "0573", "MVI_", 24, deanimation.DUAL, deanimation.ACCUMULATION, n=4, explicit_date="2018-04-02", extension="-stabilized.mov")
# s.extract()
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade()
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True, curves={"all":[[0, 0], [80, 110], [162, 255]]})

# Coral releases poseidonia
# s = Sequence(2018, "0592", "MVI_", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=25)
# s.extract(curves={"all":[[2, 0], [133, 114], [231, 255]]})
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30)

# Mosquitos over the river
# s = Sequence(2018, "0645", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1)
# s.extract(curves = {"r":[[7, 0], [91, 126], [155, 255]],
#                     "g":[[4, 0], [85, 125], [157, 255]],
#                     "b":[[9, 0], [92, 124], [167, 255]]})
# s.abridge([[20, 320]])
# s.encode_concise(source="abridged")
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", crossfade_duration=60, still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-60)
