# Deanimation

The procedural tools I've used in my deanimation artworks

www.jeremysarchet.com/deanimation

Depends upon:
* FFMPEG for video frame extraction and encoding
* Pillow for image manipulation and lighten/darken blending

Intended use of `deanimation.py`:

1. Given a source video, `extract` creates a sequence of its frames

2. Then you've got an image sequence from which `accumulation_single` creates
a single deanimation image.

3. From that same image sequence sequence, `accumulation_sequence` creates
a new special-effect image sequence which visualizes the progression of
accumulated blends.

4. Then you've got an accumulation sequence from which `encode` creates an 
accumulation video.

The recommended workflow is to use bitmap (.bmp) for the image sequences.

- .jpg has the advantage of being fast but leaving artifacts in the accumulation
- .png has the advantage of being artifact-free but taking a long time and
requiring a lot of memory due to the lossless compression required.
- .bmp is fast and artifact-free but does require a lot of storage space. 

.bmp wins out because it has the advantage of being fast and free from
compression. The directories of .bmp files can be treated as intermediary and
can be deleted upon completion.

***

An example script `process.py` is provided.
Here is the sample video used with `process.py` to create a deanimation image and visualization video.
You may right click and save/download the video and try it yourself.
![2017.001.003-sample.mp4](https://dl.dropbox.com/s/y6ie4qut3ofv0vf/2017.001.003-sample.mp4)


Here is the resulting deanimation image:

<img src="https://dl.dropbox.com/s/0mc0faq5az6mnxy/2017.001.003-final-blend.png" width="400">


And here is the resulting visualization video:
![Visualization video](https://dl.dropbox.com/s/zxiwjyvnk5rsxhs/2017.001.003-accumulation.mp4)