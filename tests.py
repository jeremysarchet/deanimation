# # Fade test
# id = "0023"
# prefix = "DSCF"
# framerate = "24"
# n, t, r = 3, 15, 5
# # keeper_intervals = [[264, 288]]
# keeper_intervals = [[200, 310]]
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [90, 100], [180, 255]]}

# prefix, id = "DSCF", "0048"
# framerate = "24"
# trim = [1, 13]
# n, t = 3, 20
# mode = deanimation.LIGHTEN
# curves = {"all":[[24, 0], [108, 157], [175, 255]]}

# prefix, id = "DSCF", "0052"
# framerate = 24
# keeper_intervals = [[250, 1790]]
# n, t = 4, 50
# mode = deanimation.DARKEN
# curves = {"all":[[64, 0], [246, 255]]}

# prefix, id = "DSCF", "0058"
# framerate = "24"
# trim = [2, 110]
# n, t = 4, 50
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [90, 104], [168, 255]]}

# prefix, id = "DSCF", "0081"
# framerate = "24"
# trim = [0, 85]
# n, t = 3, 50
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [255, 255]]}

# prefix, id = "DSCF", "0111"
# framerate = "24"
# trim = ["01:58", "02:58"]
# n, t = 3, 15
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [42, 124], [94, 255]]}

# prefix, id = "DSCF", "0150"
# framerate = "24"
# trim = [4, 94]
# n, t = 5, 30
# mode = deanimation.LIGHTEN
# curves = {"all":[[0, 0], [86, 146], [175, 255]]}

# prefix, id = "DSCF", "0152"
# framerate = "24"
# trim = [8, 92]
# n, t = 5, 30
# mode = deanimation.LIGHTEN
# curves = {"all":[[0, 0], [200, 255]]}

# prefix, id = "DSCF", "0179"
# framerate = "24"
# trim = [0, 48]
# n, t = 4, 50
# mode = deanimation.LIGHTEN
# curves = {"all":[[0, 0], [175, 255]]}

# prefix, id = "", "2019-08-12 16.38.58.mp4"
# framerate = "60"
# trim = [3, 41]
# n, t = 10, 100
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [120, 120], [235, 255]]}

# prefix, id = "DSCF", "0202"
# framerate = "24"
# trim = [1, 94]
# n, t = 4, 50
# mode = deanimation.LIGHTEN
# curves = {"all":[[6, 0], [109, 119], [211, 255]]}

# prefix, id = "DSCF", "0241"
# framerate = "24"
# trim = [4, 93]
# n, t = 7, 75
# mode = deanimation.DARKEN
# curves = {"all":[[7, 0], [81, 117], [150, 255]]}
