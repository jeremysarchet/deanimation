"""Custom blending via numpy arrays"""

import os

from PIL import Image, ImageChops, ImageFilter
import matplotlib
import numpy


def image_frombytes(data):
    """Returns a PIL friendly image from a boolean array
    From: https://stackoverflow.com/questions/50134468/convert-boolean-numpy-array-to-pillow-image
    """
    size = data.shape[::-1]
    databytes = numpy.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes)


def dual(a, b):
    """Takes two images, `a` and `b` and makes a lighten and a darken blend and
    then overlays the two"""
    lighter = ImageChops.lighter(a, b)
    darker = ImageChops.darker(a, b)

    return overlay(lighter, darker)


def combine(a, b, mask):
    """Takes two numpy arrays, `a` and `b` representing images e.g. [x y rgb]
    and combines the masked a with the inverse masked b.

    The result is that each pixel is taken either from `a` or `b` according
    to the mask.

    E.g.
    dimension1: y which ranges from [0-1079]
    dimension2: x which ranges from [0-1919]
    dimension3: color which ranges from [0-2]

    my_numpy_image[200][100][0] # get the red value of pixel at 100, 200
    """

    # Get the pixel color first in preparation for mask multiplication
    # Transpose such that the color axis is the first one
    a = a.transpose(2, 0, 1)
    b = b.transpose(2, 0, 1)

    # Combine a and b taking from either one or the other according to the mask
    # (If an int is multipled by a bool, it's equivalent to multiplying it by
    # either a 0 or a 1)
    result = (a * mask) + (b * numpy.logical_not(mask))

    # Put the pixel color back to last so it's ordered as an image
    result = result.transpose(1, 2, 0)

    return result


def overlay_sequence(sequence_a_directory, sequence_b_directory, output_directory):
    # Get a list of all the image files in the directories, exclude hidden files
    sequence_a_filenames = [f for f in os.listdir(sequence_a_directory) if not f.startswith(".")]
    sequence_b_filenames = [f for f in os.listdir(sequence_b_directory) if not f.startswith(".")]
    sequence_a_filenames.sort()
    sequence_b_filenames.sort()

    if len(sequence_a_filenames) != len(sequence_b_filenames):
        print("Something's wrong. Sequences a and and b don't match in length")
        return

    for i in range(len(sequence_a_filenames)):
        msg = "Overlaying frame {} of {}. {}".format(i, len(sequence_a_filenames), sequence_a_filenames[i])
        print(msg)
        image_a = Image.open(os.path.join(sequence_a_directory, sequence_a_filenames[i]))
        image_b = Image.open(os.path.join(sequence_b_directory, sequence_b_filenames[i]))
        overlayed_image = overlay(image_a, image_b)
        output_path = os.path.join(output_directory, sequence_a_filenames[i])
        overlayed_image.save(output_path)


def overlay(a_image, b_image):
    """From: https://stackoverflow.com/questions/52141987/overlay-blending-mode-in-python-efficiently-as-possible-numpy-opencv
    """
    a = numpy.array(a_image)
    b = numpy.array(b_image)

    a = a.astype(float)/255
    b = b.astype(float)/255 # make float on range 0-1

    mask = a >= 0.5 # generate boolean mask of everywhere a > 0.5
    ab = numpy.zeros_like(a) # generate an output container for the blended image

    # now do the blending
    ab[~mask] = (2*a*b)[~mask] # 2ab everywhere a<0.5
    ab[mask] = (1-2*(1-a)*(1-b))[mask] # else this

    # Scale to range 0..255 and save
    c = (ab*255).astype(numpy.uint8)

    return Image.fromarray(c)


def darker_color(a, b):
    a_array = numpy.array(a)
    b_array = numpy.array(b)
    weights = numpy.array([2126, 7152, 722])
    a_lum = numpy.dot(a_array, weights)
    b_lum = numpy.dot(b_array, weights)
    mask = a_lum < b_lum
    # mask_image = image_frombytes(mask)
    # mask_image.save('comparison-test/mask.png')
    result = combine(a_array, b_array, mask)
    return Image.fromarray(result)


def lighter_color(a, b):
    a_array = numpy.array(a)
    b_array = numpy.array(b)
    weights = numpy.array([2126, 7152, 722])
    a_lum = numpy.dot(a_array, weights)
    b_lum = numpy.dot(b_array, weights)
    mask = a_lum > b_lum
    result = combine(a_array, b_array, mask)
    return Image.fromarray(result)


def compare_further_from_image(a, b, image, save_mask_at_path=None):
    """Compares two numpy arrays representing images, `a` and `b`, and returns
    a new array whose pixels are either from `a` or `b` whichever one's
     value is fruther from the corresponding pixel in `image` using euclidean
    distance.

    `image` is a numpy array representing a background image to compare against

    `save_mask_at_path` is a filepath to save the mask as a black and white image.
    """
    # Convert to float in range 0-1
    a_f = a.astype(float)/255
    b_f = b.astype(float)/255
    image = image.astype(float)/255

    # Compute the distance from each pixel to the reference pixel
    weights = numpy.array([3.0, 4.0, 2.0])
    a_distance = numpy.sqrt(numpy.dot(numpy.square(image - a_f), weights))
    b_distance = numpy.sqrt(numpy.dot(numpy.square(image - b_f), weights))

    # Generate the boolean valued image based on whether the pixel from a or b is further
    mask = a_distance > b_distance

    if save_mask_at_path:
        mask_image = image_frombytes(mask)
        mask_image.save(save_mask_at_path)

    return combine(a, b, mask)


def further_from_image(a, b, image_path):
    """Blends two images a and b and returns the image whose pixels are taken
    from a or b, whichever pixel's color is further from the corresponding pixel
    in the reference image at `image_path`.
    """
    image = Image.open(image_path)
    image_array = numpy.array(image)

    a_array = numpy.array(a)
    b_array = numpy.array(b)

    c_array = compare_further_from_image(a_array, b_array, image_array)

    return Image.fromarray(c_array)


def median(directory, frame_count):
    """Given an image sequence, create a blend image via median.

    Thanks to Daniel Walsh for this idea!

    Each subpixel of the output image is the median value of all corresponding
    subpixels from the input sequence.

    Image sequence should be in a directory named `frames` with images of the
    following format and numbering convention:
        0.png, 1.png, 2.png, ...

    Specify how many images to blend with `frame_count`.
    """
    # List of numpy images
    images = []

    # Construct list of numpy images
    for i in range(1, frame_count):
        # Construct the filepath
        filepath = os.path.join(directory, "frames", "{}.png".format(i))
        print("Loading image: {}".format(filepath))

        # Initialize the pillow image and initialize a numpy arrry from it
        pillow_image = Image.open(filepath)
        numpy_image = numpy.array(pillow_image)
        images.append(numpy_image)
        pillow_image.close()

    # Stack numpy images together
    array_of_images = numpy.stack(images)

    # Compute the median across the whole 4d array of images
    print("Computing the median...")
    numpy_result = numpy.median(array_of_images, axis=0).astype(numpy.uint8)

    # Convert the numpy result to a pillow image and save
    print("Output shape: {}".format(numpy_result.shape))
    print("Output type: {}".format(numpy_result.dtype))
    result = Image.fromarray(numpy_result)
    result.save(os.path.join(directory, "median-blend-{}.png".format(frame_count)))
    result.close()


if __name__ == "__main__":
    a = Image.open('comparison-test/a.jpg')
    b = Image.open('comparison-test/b.jpg')

    c = darker_color(a, b)
    # c = dual(a, b)
    # c = further_from_average(a, b, 109)
    # c = further_from_color(a, b, [99, 106, 115])
    # c = further_from_image(a, b, "comparison-test/background.jpg")

    # Overlay test
    # a = Image.open('test/2018/normal-lighter.png')
    # b = Image.open('test/2018/normal-darker.png')
    # c = overlay(a, b)

    # Render
    # c.show()
    c.save('comparison-test/c.jpg')
