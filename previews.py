"""Production of preview images from raw videos"""

import os
import shutil

import deanimation
import media
import utilities


BASE_INPUT_DIR =     "/Users/jbs/Dropbox/deanimation/raw"
PREVIEW_DIR =        "/Users/jbs/Dropbox/deanimation/previews"
PREVIEW_PRETTY_DIR = "/Users/jbs/Dropbox/deanimation/previews-pretty"
WORKING_DIR =        "/Volumes/noguchi/scratch/previews"
# WORKING_DIR =        "/Users/jbs/deanimation-scratch-previews"

def generate(video_data, year, prefix):

    # Make the output directories if need be
    base_directories = [PREVIEW_DIR, PREVIEW_PRETTY_DIR, WORKING_DIR]
    for base_directory in base_directories:
        if not os.path.isdir(base_directory):
            os.makedirs(base_directory)

    for vid in video_data:
        year = str(year)
        input_video = os.path.join(BASE_INPUT_DIR, year, "{}{}.MOV".format(prefix, vid["sn"]))

        hours_offset = vid["hours-offset"] if "hours-offset" in vid else 0
        date = media.get_encoded_date(input_video, hours_offset)
        id = "{}-{}".format(date, vid["sn"])

        extraction_directory = os.path.join(WORKING_DIR, year, id, "frames-extracted")
        extraction_pattern = "{}-frame-%04d.bmp".format(id)

        working_directory = os.path.join(WORKING_DIR, year, id)


        preview_path = os.path.join(PREVIEW_DIR, year, "{}-preview-n{}.png".format(id, vid["n"]))

        preview_pretty_directory = os.path.join(PREVIEW_PRETTY_DIR, year)
        preview_pretty_path = os.path.join(PREVIEW_PRETTY_DIR, year, id + "-preview-pretty-n{}.png".format(vid["n"]))

        print ("\nGenerating preview {} of {} - ID: {}".format(video_data.index(vid), len(video_data), id))

        # Preview
        if not os.path.isdir(working_directory):
            os.makedirs(working_directory)
        filter = "select=not(mod(n\,{}))".format(vid["n"])
        utilities.extract(input_video, extraction_directory, filter)
        deanimation.accumulation_single(extraction_directory, preview_path, vid["mode"], vid["n"])

        # Pretty preview (auto leveled)
        if not os.path.isdir(preview_pretty_directory):
            os.makedirs(preview_pretty_directory)
        utilities.pretty_preview(preview_path, preview_pretty_path)

        # Clean up
        print("Done. Removing working directory")
        shutil.rmtree(working_directory)
