
# Seagulls at the Sants train station (iPhone)
# Seek to 112 seconds extract to 125 seconds
# Number output sequence starting at 0
# ffmpeg \
#     -ss 112 \
#     -to 125 \
#     -i '/Volumes/mondrian/Dropbox/deanimation/raw/2018/IMG_0242.MOV' \
#     -start_number 0 \
#     'sants-seagulls/frames/%d.png'

# Hurricane of geese in the California delta (Canon T2i)
# Extract first 500 frames, with color correction, downsize to 1920x1080
# ffmpeg \
#     -i '/Volumes/mondrian/Dropbox/deanimation/raw/2020/DSCF0042.MOV' \
#     -frames 500 \
#     -vf "curves=r='0.0/0.0 0.57/1.0':g='0.0/0.0 0.59/1.0':b='0.0/0.0 0.66/1.0', scale='1920x1080'" \
#     -start_number 0 \
#     'delta-geese/frames/%d.png'

# Seagulls over Banyoles lake. (Fujifilm X-T3)
# Seek to 29 seconds, extract to 33 seconds
# ffmpeg \
#     -ss 29 \
#     -to 33 \
#     -i '/Volumes/mondrian/Dropbox/deanimation/raw/2018/MVI_0385.MOV' \
#     -start_number 0 \
#     'banyoles-seagulls/frames/%d.png'
