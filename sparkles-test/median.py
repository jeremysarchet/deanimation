"""Blend a series of frames by taking the median subpixel across the series.

Refresher on numpy arrays representing images:
[x][y][c]
Each subpixel is an unsigned 8 bit integer (0-255)
[
    [ [r, g, b], [r, g, b], [r, g, b] ],
    [ [r, g, b], [r, g, b], [r, g, b] ],
    [ [r, g, b], [r, g, b], [r, g, b] ],
]

We will `stack` numpy images to make an array of images with the following axes:
[i][y][x][c]

E.g. for a series of 10, 1920x1080pixel RGB images, here is the shape:
(10, 1080, 1920, 3)
"""

import os
from PIL import Image
import numpy

def median(directory, frame_count):
    """Given an image sequence, create a blend image via median.

    Thanks to Daniel Walsh for this idea!

    Each subpixel of the output image is the median value of all corresponding
    subpixels from the input sequence.

    Image sequence should be in a directory named `frames` with images of the
    following format and numbering convention:
        0.png, 1.png, 2.png, ...

    Specify how many images to blend with `frame_count`.
    """
    # List of numpy images
    images = []

    # Construct list of numpy images
    for i in range(frame_count):
        # Construct the filepath
        filepath = os.path.join(directory, "frames", "{}.png".format(i))
        print("Loading image: {}".format(filepath))

        # Initialize the pillow image and initialize a numpy arrry from it
        pillow_image = Image.open(filepath)
        numpy_image = numpy.array(pillow_image)
        images.append(numpy_image)
        pillow_image.close()

    # Stack numpy images together
    array_of_images = numpy.stack(images)

    # Compute the median across the whole 4d array of images
    print("Computing the median...")
    numpy_result = numpy.median(array_of_images, axis=0).astype(numpy.uint8)

    # Convert the numpy result to a pillow image and save
    print("Output shape: {}".format(numpy_result.shape))
    print("Output type: {}".format(numpy_result.dtype))
    result = Image.fromarray(numpy_result)
    result.save(os.path.join(directory, "median-blend-{}.png".format(frame_count)))
    result.close()


if __name__ == "__main__":
    # median("sants-seagulls", 391)

    # median("delta-geese", 500)

    # median("banyoles-seagulls", 121)
