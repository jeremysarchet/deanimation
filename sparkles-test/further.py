"""Test for blending with `further_from_image` blend algorithm."""

import sys
from PIL import Image
import numpy

# Make `import` know about the parent directoy
sys.path.append("..")
import custom_blends


def blend(a_filename, b_filename, background_filename, output_filename, weighted=True):
    # Background numpy image
    background = numpy.array(Image.open(background_filename))

    # Load two images
    a = numpy.array(Image.open(a_filename))
    b = numpy.array(Image.open(b_filename))

    # Blend via numpy custom blend
    if weighted:
        result_array = custom_blends.compare_further_from_image(a, b, background, special_metric="weighted-euclidean")
    else:
        result_array = custom_blends.compare_further_from_image(a, b, background)

    # Save the result
    result = Image.fromarray(result_array)
    result.save(output_filename)


if __name__ == "__main__":
    # blend("sants-seagulls/frames/365.png",
    #       "sants-seagulls/frames/375.png",
    #       "sants-seagulls/median-blend-10.png",
    #       "sants-seagulls/further-from-median-10.png")

    # blend("sants-seagulls/frames/365.png",
    #       "sants-seagulls/frames/375.png",
    #       "sants-seagulls/median-blend-100.png",
    #       "sants-seagulls/further-from-median-100.png")

    # blend("sants-seagulls/frames/365.png",
    #       "sants-seagulls/frames/375.png",
    #       "sants-seagulls/median-blend-391.png",
    #       "sants-seagulls/further-from-median-391.png")

    blend("sants-seagulls/frames/365.png",
          "sants-seagulls/frames/375.png",
          "sants-seagulls/median-blend-10.png",
          "sants-seagulls/weighted-further-from-median-10.png",
          weighted=True)

    blend("sants-seagulls/frames/365.png",
          "sants-seagulls/frames/375.png",
          "sants-seagulls/median-blend-100.png",
          "sants-seagulls/weighted-further-from-median-100.png",
          weighted=True)

    blend("sants-seagulls/frames/365.png",
          "sants-seagulls/frames/375.png",
          "sants-seagulls/median-blend-391.png",
          "sants-seagulls/weighted-further-from-median-391.png",
          weighted=True)

    # blend("banyoles-seagulls/frames/90.png",
    #       "banyoles-seagulls/frames/100.png",
    #       "banyoles-seagulls/median-blend-10.png",
    #       "banyoles-seagulls/further-from-median-10.png")

    # blend("banyoles-seagulls/frames/90.png",
    #       "banyoles-seagulls/frames/100.png",
    #       "banyoles-seagulls/median-blend-50.png",
    #       "banyoles-seagulls/further-from-median-50.png")

    # blend("banyoles-seagulls/frames/90.png",
    #       "banyoles-seagulls/frames/100.png",
    #       "banyoles-seagulls/median-blend-121.png",
    #       "banyoles-seagulls/further-from-median-121.png")

    blend("banyoles-seagulls/frames/90.png",
          "banyoles-seagulls/frames/100.png",
          "banyoles-seagulls/median-blend-10.png",
          "banyoles-seagulls/weighted-further-from-median-10.png",
          weighted=True)

    blend("banyoles-seagulls/frames/90.png",
          "banyoles-seagulls/frames/100.png",
          "banyoles-seagulls/median-blend-50.png",
          "banyoles-seagulls/weighted-further-from-median-50.png",
          weighted=True)

    blend("banyoles-seagulls/frames/90.png",
          "banyoles-seagulls/frames/100.png",
          "banyoles-seagulls/median-blend-121.png",
          "banyoles-seagulls/weighted-further-from-median-121.png",
          weighted=True)
