
import deanimation
import production

# id = "0579"
# prefix = "MVI_"
# framerate = "30"
# trim = [2, 12]
# n, t = 3, 80
# mode = deanimation.LIGHTEN
# curves = {"all":[[19, 0], [217, 255]]}

# id = "0783"
# prefix = "MVI_"
# framerate = "30"
# trim = [2, 16]
# n, t = 1, 45
# mode = deanimation.LIGHTEN
# curves = {"all":[[0, 0], [255, 255]]}

# # Flock of blackbirds which all suddenly depart together
# s = Sequence(2017, "0892", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=2)
# s.extract(curves={"all":[[0, 0], [55, 75], [115, 188], [158, 255]]})
# s.abridge([[1450, 1670]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Blackbirds flying curvy paths over island tufa
# s = Sequence(2017, "0893", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=2, explicit_date="2017-10-02", extension="-stabilized.mov")
# s.extract(curves={"all":[[0, 0], [54, 90], [100, 195], [145, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# Blackbirds flying slightly chaotically over island tufa
# s = Sequence(2017, "0903", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=2)
# s.extract(curves={"all":[[0, 0], [62, 78], [131, 192], [186, 255]]})
# s.abridge([[0, 170]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True, still_end=True, crossfade_duration=60)
# s.encode_result(source="crossfaded", poster_frame=-60, sharpen=True)

# # Blackbirds coming and going from shoreline tufa
# s = Sequence(2017, "0916", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=2)
# s.extract(curves={"all":[[0, 0], [57, 84], [112, 184], [160, 255]]})
# s.abridge([[175, 360], [739, 944]], seamless=True)
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Crows which glint over Monitor Pass
# s = Sequence(2017, "0922", "MVI_", 24, deanimation.DUAL, deanimation.ACCUMULATION, n=2)
# s.extract()
# s.abridge([[45, 288]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True, still_end=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Crows over a burnt wooded ridge
# s = Sequence(2017, "0929", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=5)
# s.extract(curves={"all":[[0, 0], [46, 68], [107, 190], [146, 255]]})
# s.abridge([[0, 530]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Crows in tight banked curves over a grove
# s = Sequence(2017, "0938", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=5)
# s.extract(curves={"all":[[0, 0], [68, 127], [129, 255]]})
# s.abridge([[24, 240]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Lots of crows departing a partially burnt ridge
# s = Sequence(2017, "0941", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=5)
# s.extract(curves={"all":[[0, 0], [54, 89], [105, 192], [143, 255]]})
# s.abridge([[24, 524]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Lots and lots of crows going everywhere
# s = Sequence(2017, "0945", "MVI_", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=4)
# s.extract(curves={"all":[[0, 0], [86, 124], [165, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Drifts of fireplace ash with bits of flame
# s = Sequence(2017, "0955", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1, explicit_date="2017-10-02", extension="-stabilized.mov")
# s.extract(curves={"all":[[18, 0], [134, 85], [255, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.crossfade(source="deanimated", still_start=True, still_end=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# # Big rush of fireplace ash
# s = Sequence(2017, "0975", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1)
# s.extract(curves={"all":[[10, 0], [81, 58], [180, 194], [255, 255]]})
# s.abridge([[100, 700]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)
