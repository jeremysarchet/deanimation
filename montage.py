import os
import shutil
import subprocess

BASE_DIR = "/Users/jbs/Dropbox/deanimation/processed/2020"
# SCRATCH_DIR = "/Volumes/noguchi/scratch/delta-montage"
SCRATCH_DIR = "/Users/jbs/Desktop/montage-test"
videos = [
    "2020-01-20-0009/2020-01-20-0009-m1-s11-i12.mp4",
    "2020-01-20-0042/2020-01-20-0042-m1-s1-i80.mp4",
    "2020-01-20-0064/2020-01-20-0064-m1-s10-i10.mp4",
    "2020-01-20-0082/2020-01-20-0082-m1-s1-i100.mp4",
    "2020-01-20-0084/2020-01-20-0084-m1-s7-i30.mp4",
    "2020-01-20-0095/2020-01-20-0095-m1-s7-i30.mp4",
    "2020-01-20-0097/2020-01-20-0097-m1-s5-i50.mp4",
    "2020-01-20-0100/2020-01-20-0100-m1-s1-i100.mp4",
    "2020-01-20-0141/2020-01-20-0141-accumulation-n5.mp4",
    "2020-01-20-0155/2020-01-20-0155-m1-s4-i160.mp4",
    "2020-01-20-0157/2020-01-20-0157-m1-s1-i40.mp4",
    "2020-01-20-0160/2020-01-20-0160-m1-s1-i320.mp4",
    "2020-01-20-0162/2020-01-20-0162-m1-s1-i150.mp4",
    "2020-01-20-0169/2020-01-20-0169-m1-s5-i75.mp4",
]

# BASE_DIR = "/Users/jbs/Dropbox/deanimation/processed/2019"
# SCRATCH_DIR = "/Users/jbs/deanimation-scratch/tamalpais-montage"
# videos = ["2019-12-31-0486/2019-12-31-0486-tail-n1-t120.mp4",
#           "2019-12-31-0471/2019-12-31-0471-tail-n1-t60.mp4",
#           "2019-12-31-0499/2019-12-31-0499-tail-n3-t120.mp4",
#           "2019-12-31-0460/2019-12-31-0460-tail-n2-t30.mp4",
#           "2019-12-31-0492/2019-12-31-0492-accumulation-n2.mp4",
#           "2019-12-31-0493/2019-12-31-0493-tail-n1-t120.mp4",
#           "2019-12-31-0512/2019-12-31-0512-tail-n1-t480.mp4",
#           "2019-12-31-0470/2019-12-31-0470-tail-n1-t20.mp4",
#           "2019-12-31-0503/2019-12-31-0503-tail-n2-t120.mp4"]


# BASE_DIR = "/Users/jbs/Dropbox/deanimation/processed/2018"
# SCRATCH_DIR = "/Users/jbs/deanimation-scratch/tudela-montage"
# videos = ["2018-03-21-0100/2018-03-21-0100-accumulation-n3.mp4",
#           "2018-03-22-0242/2018-03-22-0242-accumulation-n4.mp4",
#           "2018-03-24-0139/2018-03-24-0139-accumulation-n1.mp4",
#           "2018-03-25-0152/2018-03-25-0152-accumulation-n1.mp4",
#           "2018-04-02-0543/2018-04-02-0543-accumulation-n4.mp4",
#           "2018-04-02-0544/2018-04-02-0544-accumulation-n4.mp4",
#           "2018-04-02-0556/2018-04-02-0556-accumulation-n4.mp4",
#           "2018-04-02-0564/2018-04-02-0564-accumulation-n4.mp4",
#           "2018-04-02-0573/2018-04-02-0573-accumulation-n4.mp4",
#           "2018-04-03-0645/2018-04-03-0645-accumulation-n1.mp4"]

# BASE_DIR = "/Users/jbs/Dropbox/deanimation/processed/2017"
# SCRATCH_DIR = "/Users/jbs/deanimation-scratch/mono-montage"
# videos = ["2017-10-02-0903/2017-10-02-0903-accumulation-n2.mp4",
#           "2017-10-02-0892/2017-10-02-0892-accumulation-n2.mp4",
#           "2017-10-02-0922/2017-10-02-0922-accumulation-n2.mp4",
#           "2017-10-03-0975/2017-10-03-0975-accumulation-n1.mp4",
#           "2017-10-02-0916/2017-10-02-0916-accumulation-n2.mp4",
#           "2017-10-02-0941/2017-10-02-0941-accumulation-n5.mp4",
#           "2017-10-02-0955/2017-10-02-0955-accumulation-n1.mp4",
#           "2017-10-02-0945/2017-10-02-0945-accumulation-n4.mp4"]


def concatenate_with_crossfade(a_path, b_path, output_path, crossfade_duration=3):

    """
    Probe invocation adapted from: https://stackoverflow.com/a/24488789
    Crossfade invocation adapted from: https://superuser.com/a/1001040
    """

    probe = 'ffprobe \
    -v quiet \
    -print_format compact=print_section=0:nokey=1:escape=csv \
    -show_entries format=duration \
    {a_path} \
    '.format(a_path = a_path)

    a_duration = float(subprocess.check_output(probe, shell=True))

    concat = 'echo crossfade duration: {crossfade_duration}; \
    echo clip a duration: {a_duration}; \
    echo clip a crossfade start: {a_crossfade_start}; \
    \
    ffmpeg -i {a_path} -i {b_path} -an \
    -hide_banner \
    -filter_complex "\
        [0:v]trim=start=0:end={a_crossfade_start},setpts=PTS-STARTPTS[firstclip]; \
        [1:v]trim=start={crossfade_duration},setpts=PTS-STARTPTS[secondclip]; \
        [0:v]trim=start={a_crossfade_start}:end={a_duration},setpts=PTS-STARTPTS[fadeoutsrc]; \
        [1:v]trim=start=0:end={crossfade_duration},setpts=PTS-STARTPTS[fadeinsrc]; \
        [fadeinsrc]format=pix_fmts=yuva420p, \
            fade=t=in:st=0:d={crossfade_duration}:alpha=1[fadein]; \
        [fadeoutsrc]format=pix_fmts=yuva420p, \
            fade=t=out:st=0:d={crossfade_duration}:alpha=1[fadeout]; \
        [fadein]fifo[fadeinfifo]; \
        [fadeout]fifo[fadeoutfifo]; \
        [fadeoutfifo][fadeinfifo]overlay[crossfade]; \
        [firstclip][crossfade][secondclip]concat=n=3[output] \
    " \
    -map [output] -pix_fmt yuv420p {output_path}\
    '.format(a_path = a_path,
             b_path = b_path,
             crossfade_duration = crossfade_duration,
             a_duration = a_duration,
             a_crossfade_start = a_duration - crossfade_duration,
             output_path = output_path,
             )

    subprocess.call(concat, shell=True)


if not os.path.isdir(SCRATCH_DIR):
    os.makedirs(SCRATCH_DIR)


for i, video in enumerate(videos):
    print(i, video)
    current_path = os.path.join(BASE_DIR, video)
    if i == 0:
        # Split the 0th video into two parts, the head will end up concatenated at the very end
        subprocess.call(["ffmpeg", "-hide_banner", "-loglevel", "panic", "-t", "3", "-i", current_path, os.path.join(SCRATCH_DIR, "0_head.mp4")]) # The head, which gets sent to the end
        subprocess.call(["ffmpeg", "-hide_banner", "-loglevel", "panic", "-ss", "3", "-i", current_path, os.path.join(SCRATCH_DIR, "0.mp4")]) # The tail, which gets sent to the beginning
    else:
        previous_path = os.path.join(SCRATCH_DIR, "{}.mp4".format(i - 1))
        output_path = os.path.join(SCRATCH_DIR, "{}.mp4".format(i))
        concatenate_with_crossfade(previous_path, current_path, output_path)

# Concatenate the head at the very end
previous_path = os.path.join(SCRATCH_DIR, "{}.mp4".format(len(videos) - 1))
current_path = os.path.join(SCRATCH_DIR, "0_head.mp4")
output_path = os.path.join(SCRATCH_DIR, "out.mp4")
concatenate_with_crossfade(previous_path, current_path, output_path)
