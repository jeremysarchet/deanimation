"""Experiment with averaging a group of images."""

import os
from PIL import Image
import numpy

frame_count = 50
directory = "poplar-breeze"

# List of numpy images
images = []

for i in range(frame_count):
    # Construct the filepath
    filepath = os.path.join(directory, "frames", "{}.png".format(i))
    print("Loading image: {}".format(filepath))

    # Initialize the pillow image and initialize a numpy arrry from it
    pillow_image = Image.open(filepath)
    numpy_image = numpy.array(pillow_image)
    images.append(numpy_image)
    pillow_image.close()

# Stack numpy images together
array_of_images = numpy.stack(images)

# Compute the average across the whole 4d array of images
print("Computing the average...")
numpy_result = numpy.average(array_of_images, axis=0).astype(numpy.uint8)

# Convert the numpy result to a pillow image and save
result = Image.fromarray(numpy_result)
result.save(os.path.join(directory, "average-blend-{}.png".format(frame_count)))
result.close()
