"""Experiment with running average image sequence."""

import sys
import os
from PIL import Image
import numpy

trail_count = int(sys.argv[1])
print("Trail count: {}".format(trail_count))

frame_count = 300

directory = "poplar-breeze"
output_directory = os.path.join(directory, "blended-{}".format(trail_count))
trail_images = []


def load(filepath):
    """Load the image, and initialize and return a numpy array from it.."""
    pillow_image = Image.open(filepath)
    numpy_image = numpy.array(pillow_image)
    pillow_image.close()
    return numpy_image


def save(numpy_image, i):
    # Convert the numpy result to a pillow image and save
    result = Image.fromarray(numpy_image)
    result.save(os.path.join(output_directory, "{}.png".format(i)))
    result.close()


# Load and save the initial trail
for i in range(trail_count):
    filepath = os.path.join(directory, "frames", "{}.png".format(i))
    print("Loading image into trail: {}".format(filepath))
    trail_images.append(load(filepath))

# Stack numpy images together and compute and save the average
trail_array = numpy.stack(trail_images)
print("Computing the average...")
numpy_result = numpy.average(trail_array, axis=0).astype(numpy.uint8)
save(numpy_result, trail_count - 1)

# Iterate through the sequence, enqueuing/dequeuing from the trail each step
for i in range(trail_count, frame_count):
    filepath = os.path.join(directory, "frames", "{}.png".format(i))
    print("Enqueuing image into trail: {}".format(filepath))

    new_image = load(filepath)
    trail_array = numpy.delete(trail_array, 0, axis=0)
    trail_array = numpy.insert(trail_array, len(trail_array), new_image, axis=0)

    print("Computing the average...")
    numpy_result = numpy.average(trail_array, axis=0).astype(numpy.uint8)

    save(numpy_result, i)
