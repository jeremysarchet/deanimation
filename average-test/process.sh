# Close up of leaves of poplar tree swaying in the breeze 60fps, 720x1280.

# Extract
# ffmpeg \
#     -ss 2 \
#     -to 7 \
#     -i '/Users/jbs/Downloads/MVI_0654.MOV' \
#     -start_number 0 \
#     'poplar-breeze/frames/%d.png'

# Encode original
# ffmpeg \
#     -i 'poplar-breeze/frames/%d.png' \
#     -r 24 \
#     -pix_fmt 'yuv420p' \
#     'poplar-breeze/poplar-breeze-original.mp4'

# Setup some variables
trail_count=10
averaged_dir="poplar-breeze/blended-${trail_count}"

# Average
rm -r $averaged_dir
mkdir $averaged_dir
python3 running-average.py $trail_count

# Encode processed
ffmpeg \
    -start_number $((trail_count - 1)) \
    -i "poplar-breeze/blended-${trail_count}/%d.png" \
    -r 24 \
    -pix_fmt yuv420p \
    "poplar-breeze/poplar-breeze-averaged-t${trail_count}.mp4"
