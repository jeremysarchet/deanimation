"""Helper functions pertaining to media information"""

from datetime import datetime, timedelta
from pymediainfo import MediaInfo


def get_encoded_date(video_path, hours_offset=0):
    media_info = MediaInfo.parse(video_path)

    for track in media_info.tracks:
        if track.track_type == 'Video':
            encoded_date = track.encoded_date[4:]
            datetime_object = datetime.strptime(encoded_date, "%Y-%m-%d %H:%M:%S")
            corrected_date = datetime_object + timedelta(hours=hours_offset)
            return str(datetime.strftime(corrected_date, "%Y-%m-%d"))


def get_resolution(video_path):
    media_info = MediaInfo.parse(video_path)

    for track in media_info.tracks:
        if track.track_type == 'Video':
            return track.width, track.height

            # # All the data
            # data = track.to_data()
            # for key in data:
            #     print(key, data[key])


if __name__ == "__main__":
    video_path = '/Users/jbs/Dropbox/deanimation/raw/2019/DSCF0492.MOV'
    print(get_resolution(video_path))
