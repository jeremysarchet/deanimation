
import deanimation
from production import Sequence

# # Four pigeons
# sn, prefix, framerate = "0031", "DSCF", 24
# year, hours_offset = 2019, 9
# keeper_intervals = [[8 * 24, 23 * 24]]
# n, t = 3, 40
# offset = 110
# poster_frame = 202
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [85, 100], [170, 255]]}

# # Three birds make a close pass reflected in water
# sn, prefix, framerate = "0076", "DSCF", 24
# year, hours_offset = 2019, 9
# keeper_intervals = [[60, 390]]
# n, t = 3, 30
# poster_frame = 100
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [42, 110], [82, 255]]}
# crossfade_duration = 48

# # Golden swallow
# sn, prefix, framerate = "0119", "DSCF",  24
# year, hours_offset = 2019, 9
# keeper_intervals = [[120, 2496]]
# n, t = 6, 75
# poster_frame = 918
# mode = deanimation.DARKEN
# curves =   {"r":[[0, 0], [57, 102], [115, 255]],
#             "g":[[0, 0], [73, 102], [147, 255]],
#             "b":[[0, 0], [83, 102], [164, 255]]}
# offset = 1434

# # Roman bridge bugs and droplets
# sn, prefix, framerate = "0206", "DSCF", 24
# year, hours_offset = 2019, 9
# keeper_intervals = [[0, 82 * 24]]
# n, t = 4, 50
# poster_frame = 292
# mode = deanimation.LIGHTEN
# curves = {"all":[[0, 0], [94, 108], [190, 255]]}

# # Swallows and bat with fog and moon
# sn, prefix, framerate = "0221", "DSCF", 24
# year, hours_offset = 2019, 9
# keeper_intervals = [[826, 3418]]
# n, t = 5, 100
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [95, 100], [195, 255]]}
# offset = 1010

# # Bats surrounded by foliage
# sn, prefix, framerate = "0263", "DSCF", 24
# year, hours_offset = 2019, 9
# keeper_intervals = [[210, 330], [650, 715], [1110, 1192], [1270, 1570], [1820, 1995], [2120, 2270], [2420, 2530], [2605, 2850], [3170, 3300], [3595, 3891]]
# n, t = 4, 80
# poster_frame = 550
# mode = deanimation.DARKEN
# crossfade_duration = 24
# curves = {"all":[[0, 0], [78, 192], [146, 255]]}
# offset = 440

# # Bats with a bit of foliage in the right corners
# sn, prefix, framerate = "0265", "DSCF", 24
# year, hours_offset = 2019, 9
# n, t = 5, 50
# poster_frame = 1320 - 440
# keeper_intervals = [[520, 777], [825, 914], [1288, 1470], [1948, 2184], [2394, 2484], [2626, 2831], [2995, 3157], [3293, 3453], [3534, 3610]]
# crossfade_duration = 24
# mode = deanimation.DARKEN
# curves = {"all":[[7, 0], [84, 156], [167, 255]]}
# offset = 440

# # Lots of swallows in front of a blue hole of sky
# sn, prefix, framerate = "0301", "DSCF", 24
# year, hours_offset = 2019, 9
# n = 5
# crossfade_duration = 5 * 24
# poster_index = -crossfade_duration
# keeper_intervals = [[880, 2140]]
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [117, 144], [235, 255]]}

# # Field bats in front of a pink and purple sky
# sn, prefix, framerate = "0330", "DSCF", 24
# year, hours_offset = 2019, 9
# n, t = 4, 40
# poster_frame = 2085 - 848
# crossfade_duration = 15
# keeper_intervals = [[30, 352], [645, 692], [1070, 1152], [1300, 1440], [1512, 1863], [1900, 2470], [2638, 2689], [2910, 3325], [3520, 3890], [4284, 4505], [4595, 4877]]
# mode = deanimation.DARKEN
# curves = {"all":[[0, 0], [49, 152], [90, 255]]}
# offset = 848

# # Vaulting bugs on the rivertop
# sn, prefix, framerate = "0363", "DSCF", 24
# year, hours_offset = 2019, 9
# n, t = 4, 80
# mode = deanimation.LIGHTEN
# keeper_intervals = [[24 * 7, 2161 - 24 * 7]]
# curves =   {"r":[[10, 0], [55, 47], [210, 255]],
#             "g":[[10, 0], [210, 255]],
#             "b":[[10, 0], [210, 255]]}

# Wall of birds between two bare trees
# s = Sequence(2019, "0427", "DSCF", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=1)
# s.encode_raw_preview()
# s.extract()
# s.abridge([[265, 2250]])
# s.deanimation_still(source="abridged")

# Bird freeway diagonal
# s = Sequence(2019, "0432", "DSCF", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=1, t=50)
# s.encode_raw_preview()
# s.extract(curves = {"all":[[12, 0], [206, 255]]})
# s.abridge([[265, 1496]])
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", crossfade_duration=72)
# s.encode_result(source="crossfaded", poster_frame=-72)

# View of Sacramento from the West closeup
# s = Sequence(2019, "0435", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=2, t=40)
# s.encode_raw_preview()
# s.extract(curves= {"all": [[0, 0], [78, 69], [153, 190], [207, 255]]})
# s.abridge([[75, 900]]) # remove camera shake
# s.abridge([[276, 900]]) # remove low-flying birds
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", crossfade_duration=72)
# s.encode_result(source="crossfaded", poster_frame=-72)


# View of Sacramento from the West wide
# s = Sequence(2019, "0437", "DSCF", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=2)
# s = Sequence(2019, "0437", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=2, t=120)
# s.encode_raw_preview()
# s.extract(curves= {"all": [[0, 0], [73, 79], [144, 188], [194, 255]]})
# s.abridge([[75, 1070]]) # remove camera shake
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", crossfade_duration=72)
# s.encode_result(source="crossfaded", poster_frame=-72)


# Bug swarms above a tree at the trailhead down to Muir Woods 2k
# s = Sequence(2019, "0460", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=30)
# s.encode_raw_preview()
# s.extract(curves={"all": [[0, 0], [68, 63], [166, 193], [225, 255]]})
# s.abridge([[220, 830]])
# s.encode_concise(sharpen=True)
# s.deanimate()
# s.encode_result(sharpen=True)
# s.crossfade()
# s.encode_result(source="crossfaded", sharpen=True)

# Bug swarms above a tree at the trailhead down to Muir Woods 4k
# s = Sequence(2019, "0461", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=30)
# s.encode_raw_preview()
# s.extract(curves={"all": [[0, 0], [61, 71], [126, 186], [176, 255]]})
# s.abridge([[0, 500]])
# s.encode_concise()
# s.deanimate()
# s.crossfade()
# s.encode_result(source="crossfaded")

# Mist which appears homogenous when fully accumulated 2k
# s = Sequence(2019, "0470", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=20)
# s.encode_raw_preview()
# s.extract(curves={"all": [[93, 0], [215, 255]]}, saturation=0.8)
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(sharpen=True)
# s.crossfade()
# s.encode_result(source="crossfaded")

# Mist with fast straight streaks in foreground
# s = Sequence(2019, "0471", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=60)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[78, 0], [117, 73], [160, 184], [196, 255]],
#                     "g":[[70, 0], [111, 72], [158, 191], [192, 255]],
#                     "b":[[74, 0], [111, 66], [160, 188], [196, 255]]},
#           saturation = 0.8)
# s.abridge([[220, 1320]])
# s.encode_concise()
# s.deanimate()
# s.encode_result()
# s.crossfade()
# s.encode_result(source="crossfaded")

# Mist with some amounts of wildness 4k
# s = Sequence(2019, "0473", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=100)
# s.encode_raw_preview()
# s.extract(curves={"all": [[47, 0], [174, 255]]}, saturation=0.8)
# s.abridge([[500, 1000]])
# s.deanimation_still(source="abridged")
# s.encode_concise(source="extracted")
# s.deanimate(source="abridged")
# s.crossfade()
# s.encode_result(source="crossfaded")

# Mist and water droplets in front of broad redwood trunk 2k
# s = Sequence(2019, "0486", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=120)
# s.encode_raw_preview()
# s.extract(curves={"all": [[33, 0], [105, 72], [183, 172], [255, 255]]})
# s.abridge([[220, 2100]])
# s.encode_concise()
# s.deanimate()
# s.encode_result()
# s.crossfade()
# s.encode_result(source="crossfaded")

# Medium sized redwood trunks with droplets and bugs 2k
# Note: concatenated with the frames from 0491
# s = Sequence(2019, "0490", "DSCF", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1, explicit_date="2019-12-31", extension="-0491-concat.mp4")
# s.encode_raw_preview()
# s.extract(curves={"all": [[14, 0], [59, 82], [102, 181], [142, 255]]})
# s.multiplex(5, source="extracted")
# s.encode_concise(source="multiplexed", sharpen=True)
# s.deanimate(source="multiplexed")
# s.encode_result(sharpen=True, poster_frame=-1)

# Moss-covered diagonal trunk with mist and droplets
# s = Sequence(2019, "0492", "DSCF", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=2)
# s.encode_raw_preview()
# s.extract(curves={"all": [[10, 0], [91, 109], [166, 255]]})
# s.abridge([[600, 719], [0, 300]])
# s.multiplex(2, source="abridged")
# s.level(-3, source="multiplexed")
# s.encode_concise(source="leveled", sharpen=True)
# s.deanimation_still(source="leveled")
# s.deanimate(source="leveled")
# s.encode_result(poster_frame=-1)
# s.crossfade(still_end=True, crossfade_duration=24)
# s.encode_result(source="crossfaded", sharpen=True, poster_frame=-24)

# Swirl of bubbles from directly above 2k
# s = Sequence(2019, "0493", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=120)
# s.encode_raw_preview()
# s.extract(curves={"all": [[20, 0], [89, 85], [158, 187], [209, 255]]})
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")
# s.crossfade()
# s.encode_result(source="crossfaded")

# Sheet of bubbles from right to left 2k
s = Sequence(2019, "0499", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=3, t=120, r=120)
s.encode_raw_preview()
s.extract(curves = {"r":[[70, 0], [127, 66], [200, 187], [255, 255]],
                    "g":[[74, 0], [132, 73], [200, 183], [255, 255]],
                    "b":[[106, 0], [150, 103], [215, 178], [255, 255]]})
s.deanimation_still()
s.deanimate(source="extracted")
s.encode_result()
s.crossfade()
s.encode_result(source="crossfaded")

# Diagonal view of river with fern and branch on banks 2k
# s = Sequence(2019, "0503", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=120)
# s.encode_raw_preview()
# s.extract(curves={"all": [[25, 0], [115, 77], [192, 182], [255, 255]]})
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")
# s.crossfade()
# s.encode_result(source="crossfaded")

# Frame-filled view of fairly laminar flow 2k
# s = Sequence(2019, "0512", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=480)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[76, 0], [239, 255]],
#                     "g":[[76, 0], [238, 255]],
#                     "b":[[81, 0], [255, 255]]})
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")
# s.crossfade()
# s.encode_result(source="crossfaded")
